Projeto com hello world simples em spring boot.

Criado para fazer comparação com Quarkus

## Building docker image
docker image build -f src/Dockerfile -t <nome_imagem> .

## Run Container
docker container run -d -p 8080:8080 <nome_imagem>