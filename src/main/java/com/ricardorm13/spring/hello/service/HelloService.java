package com.ricardorm13.spring.hello.service;

import org.springframework.stereotype.Repository;

@Repository
public class HelloService {
    public String hello(){
        return "Hello World";
    }

    public String hello(String nome){
        return "Olá " + nome;
    }
}
