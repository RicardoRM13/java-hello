package com.ricardorm13.spring.hello.controller;

import com.ricardorm13.spring.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControlerHello {
    @Autowired
    private HelloService servico;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String GetByNome() {
        try{
            return servico.hello();
        }catch (Exception e){
            System.out.println("Erro: " + e);
            return null;
        }
    }

    @RequestMapping(value = "/hello/{nome}", method = RequestMethod.GET)
    public String HelloName(@PathVariable(value = "nome") String nome)
    {
        try{
            return servico.hello(nome);
        }catch (Exception e){
            System.out.println("Erro: " + e);
            return null;
        }
    }
}
